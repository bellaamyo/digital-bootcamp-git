/*
 * Create By : Bella Amy Octaviani
 * Create At : 12 Juni 2023
 */
package Inheritance;

/**
 *
 * @author BELLA
 */
public class Programmer extends Person{
    public String teknologi;
    
    public Programmer(){
        
    }
    public Programmer(String name, String addres, String teknologi){
        super(name, addres);
        this.teknologi = teknologi;
    }
   public  void hacking(){
        System.out.println("Sya dapat merestas apapun");
    }
    public void coding(){
        System.out.println("Saya dapat membuat apapun");
    }
    public void greeting(){
      System.out.println("Hello, My Name is " + name + ". ");
      System.out.println("I, Come from " + addres + ". ");
      System.out.println("I am a " + teknologi + "Programer");
  }
}
