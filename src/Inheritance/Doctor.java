/*
 * Create By : Bella Amy Octaviani
 * Create At : 12 Juni 2023
 */
package Inheritance;

/**
 *
 * @author BELLA
 */
public class Doctor extends Person{
    public String spesialis;
    
    public Doctor(){
        
    }
    public Doctor(String name, String Addres, String spesialis){
        super();
        this.spesialis = spesialis;
        
    }
    
    public void periksa(){
        System.out.println("Sya dapat memeriksa apapun " + spesialis + ". ");
    }
    public void greeting(){
        super.greeting();
      System.out.println("Hello, My Name is : " + name + ". ");
      System.out.println("I, Come from : " + Addres + ". ");
      System.out.println("My spesialis is : " + spesialis + "Doctor");
  }
}
